

Write​ ​ansible​ ​playbook,​ ​using​ ​roles​ ​that​ ​will:

-  Spawn​ ​2​ ​local​ ​docker​ ​containers

-  Container​ ​one​ ​should​ ​have​ ​​docker_root​​ ​user​ ​with​ ​automatically​ ​generated​ ​key​ ​pair

-  Container​ ​one​ ​should​ ​have​ ​user​ ​provided​ ​SSH-public​ ​key​ ​(as​ ​a​ ​variable)​ ​added​ ​to

docker_root​ ​​user​ ​authorized_keys.

-  Container​ ​two​ ​should​ ​allow​ ​to​ ​SSH​ ​from​ ​container​ ​one​ ​using​ d​​ocker_root​ ​​user​ ​and​ ​the

generated​ ​keys

-  Check​ ​the​ ​connectivity​ ​from​ ​container​ ​one​ ​to​ ​container​ ​two​ ​in​ ​automated​ ​way.
