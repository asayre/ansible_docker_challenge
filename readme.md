The idea for [this workshop](https://www.meetup.com/it-ntl/events/247750297/) came from an IT engineer friend of mine. He was moving to Germany and looking for a job there. One of the places he applied gave him a take-home skills assessment that included the following challenge:

>Write​ ​ansible​ ​playbook,​ ​using​ ​roles​ ​that​ ​will:
>-  Spawn​ ​2​ ​local​ ​docker​ ​containers
>-  Container​ ​one​ ​should​ ​have​ ​​docker_root​​ ​user​ ​with​ ​automatically​ ​generated​ ​key​ ​pair
>-  Container​ ​one​ ​should​ ​have​ ​user​ ​provided​ ​SSH-public​ ​key​ ​(as​ ​a​ ​variable)​ ​added​ ​to: **docker_root​ ​​user​ ​authorized_keys**
>-  Container​ ​two​ ​should​ ​allow​ ​to​ ​SSH​ ​from​ ​container​ ​one​ ​using​ d​​ocker_root​ ​​user​ ​and​ ​the generated​ ​keys
>-  Check​ ​the​ ​connectivity​ ​from​ ​container​ ​one​ ​to​ ​container​ ​two​ ​in​ an ​automated​ ​way

My friend was not sure how to do this, had never attempted anything like this, and so was asking me and several other friends in a chat room we are all part of.

I also didn't know how to do this. Neither did anyone else in our group. Furthermore, none of us understood why anyone would WANT to do this. It violates EVERYTHING I've been taught about containers: SSH to an interactive session inside containers and connect them to each other using normal SSH key-pair authentication? As if the containers were VMs or physical Linux hosts? Why not use docker-compose or Kubernetes for this? And why use Ansible to spin up Docker images when Docker has its own YAML scripting automation? Very mysterious indeed, but here is some company in Germany asking for this in a skills test. Perhaps they are just not a very sophisticated IT shop. We dismissed the whole thing with snarky remarks and went back to making snarky remarks about other stuff.

BUT, I kept thinking about it... lets assume the purpose of this challenge was simply to assess creative problem solving skills. As well as spark some conversation about supposed "right" or "wrong" ways to do certain things. Taking on this challenge and then being able to talk about other more appropriate ways to accomplish the same thing might illuminate what a job candidate knows and does not know about Ansible, Docker, and Linux basics.

So that's why were here today. Let's take this challenge seriously and see what comes of it eh?

I started by googling to see who else had done something like this and found the work of Chris Myers:
* https://www.ansible.com/blog/testing-ansible-roles-with-docker
* https://github.com/chrismeyersfsu/

So I based my solution on his work.

---

# Follow the below steps and you too can connect containers in a weird inappropriate way!

* using a Virtualbox Ubuntu/Centos guest as the working host, create and use a passwordless sudoer user

	* https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-centos-quickstart
	* https://www.tecmint.com/run-sudo-command-without-password-linux/


* once you are logged in as your passwordless sudoer user, create ssh key-pair using defaults: `ssh-keygen`
	* NOTE - its important that the keys get saved to the default name and directory: `~/.ssh/id_rsa`


* required packages (yum or apt):

	* git
	* docker
	* ansible (for centos, epel extras also required)
	* python-docker-py or python-docker depending on distro
	* sshpass


* git clone this repo:

    * `git clone git@gitlab.com:asayre/ansible_docker_challenge.git`

* then go to the main folder: `cd ansible_docker_challenge/`

	* NOTE - all ansible playbooks must be run from `ansible_docker_challenge/`

* change the password on vault encrypted file called 'ansible_pass':

	* `ansible-vault rekey ansible_pass`

* you will be prompted to change the password (current password=jiveturkey, change it to something else, don't lose it!)
	* NOTE: the contents of this file will become the user password (root and docker_root) when dockerbuild.yml playbook is run

* edit the contents of the file if you don't want the default:
	* `ansible-vault edit ansible_pass`


* open dockerbuild.yml playbook, change the user, owner, and group to your passwordless sudoer username, save


* run your newly modified ansible playbook to build your Dockerfile and then your new image: `ansible-playbook dockerbuild.yml --ask-vault-pass`

* open spin_up_docker.yml playbook, change the user to your passwordless sudoer username, save

* run your newly modified ansible playbook to create new containers from the image you created in the previous step: `ansible-playbook spin_up_docker.yml`

* you should now have two docker containers with names specified in the "vars:inventory" section of spin_up_docker.yml

* verify this: `sudo docker ps`

* you should be able to ssh into each using docker_root user without needing a user password, you may still need to supply a private key password, if you chose to do that at the `ssh-keygen` step, which is recommended for security

* verify this: `ssh docker_root@[container_name]`
